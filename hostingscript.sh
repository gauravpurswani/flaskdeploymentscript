sudo apt update
sudo apt install python3-pip python3-dev build-essential libssl-dev libffi-dev python3-setuptools gunicorn nginx -y
pip install wheel
pip install flask
sudo ufw allow 5000
sudo cp ./myproject.service.txt /etc/systemd/system/myproject.service
sudo cp ./nginxconfigfile.txt /etc/nginx/sites-available/myproject
sudo rm /etc/nginx/sites-enabled/myproject
sudo ln -s /etc/nginx/sites-available/myproject /etc/nginx/sites-enabled
sudo systemctl daemon-reload
sudo systemctl start myproject
sudo systemctl enable myproject
sudo systemctl status myproject
sudo nginx -t
sudo systemctl restart nginx
sudo ufw delete allow 5000
sudo ufw allow 'Nginx Full'

