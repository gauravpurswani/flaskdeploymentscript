[Unit]
Description=Gunicorn instance to serve myproject
After=network.target

[Service]
User=ubuntu
Group=www-data
WorkingDirectory=/home/ubuntu/flask-app
#Environment="PATH=/home/sammy/myproject/myprojectenv/bin"
ExecStart=/usr/bin/gunicorn --workers 3 --bind 0.0.0.0:5000 -m 007 wsgi:app --chdir /home/ubuntu/flask-app/
